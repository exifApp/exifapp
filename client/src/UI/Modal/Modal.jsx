import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

const useStyles = makeStyles(theme =>
  createStyles({
    paper: {
      position: 'absolute',
      top: '0',
      left: '0',
      right: '0',
      bottom: '0',
      margin: 'auto',
      width: 400,
      height: 'fit-content',
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3)
    }
  })
);

export default function ModalUI(props) {
  const classes = useStyles({});

  return (
    <Modal open={props.open} onClose={props.close}>
      <div className={classes.paper}>{props.children}</div>
    </Modal>
  );
}
