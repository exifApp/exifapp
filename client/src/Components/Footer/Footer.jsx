import React from 'react';
import styled from 'styled-components';
import RoomIcon from '@material-ui/icons/Room';
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TwitterIcon from '@material-ui/icons/Twitter';
import GitHubIcon from '@material-ui/icons/GitHub';

const Container = styled.div`
  height: 400px;
  color: #fefefe;
  background-color: #282c2f;
  display: flex;
  @media (max-width: 768px) {
    width: 80%;
  }
`;
const ContentContainer = styled.div`
  margin: 0 auto;
  display: flex;
  justify-content: center;
`;
const AboutCompany = styled.div`
  width: 40%;
  margin: auto 0;
`;

const AboutDescription = styled.div`
  width: 40%;
  margin: auto 0;
`;
const LeftSideContent = styled.div`
  display: flex;
  margin: 1.5rem 0rem;
`;
const Description = styled.div`
  display: flex;
  flex-direction: column;
`;
const SocialContainer = styled.div`
  display: flex;
  margin: 1rem 1rem;
`;
const Footer = () => {
  return (
    <Container>
      <ContentContainer>
        <AboutCompany>
          <LeftSideContent>
            <RoomIcon fontSize="large" style={{ margin: 'auto 1rem' }} />
            <Description>
              <b>21 Revolution Street</b> Szczecin, Bazarowa Street
            </Description>
          </LeftSideContent>
          <LeftSideContent>
            <PhoneIcon fontSize="large" style={{ margin: 'auto 1rem' }} />
            <Description>+47 372 273 127</Description>
          </LeftSideContent>
          <LeftSideContent>
            <MailIcon fontSize="large" style={{ margin: 'auto 1rem' }} />
            <Description>J.97KOWALSKI@GMAIL.COM</Description>
          </LeftSideContent>
        </AboutCompany>
        <AboutDescription>
          <h3>About Componany </h3>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
          been the industry's standard dummy text ever since the 1500s, when an unknown printer took
          a galley of type and scrambled it to make a type specimen book. It has survived not only
          <SocialContainer>
            <FacebookIcon fontSize="large" style={{ margin: 'auto 1rem' , cursor : 'pointer' }} />
            <LinkedInIcon fontSize="large" style={{ margin: 'auto 1rem', cursor : 'pointer' }} />
            <TwitterIcon fontSize="large" style={{ margin: 'auto 1rem', cursor : 'pointer' }} />
            <GitHubIcon fontSize="large" style={{ margin: 'auto 1rem', cursor : 'pointer' }} />
          </SocialContainer>
        </AboutDescription>
      </ContentContainer>
    </Container>
  );
};
export default Footer;
