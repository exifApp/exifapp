import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import Loading from '../../UI/Loading/Loading';
import { useHistory } from 'react-router-dom';
import { axiosInstance } from '../../axios';
export const ContainerLogin = styled.form`
  margin: 0 auto;
  height: 100%;
  width: 500px;
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    width: auto;
  }
`;
const Input = styled.input`
  margin: 0.5em;
  padding: 1em;
  background-color: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
  font-size: 18px;
`;
const HeaderNav = styled.h1`
  font-family: 'Muli', sans-serif;
  margin: 10px auto;
  font-weight: 300;
  @media (max-width: 768px) {
    padding-left: 0.5em;
  }
`;
export const LoginButton = styled.button`
  color: #fff;
  border-color: #2e6da4;
  padding: 1em;
  margin-top: 1em;
  border-radius: 3px;
  border: none;
  background: ${props => props.theme.main};
`;
const WrapperInfo = styled.div`
  display: 'flex';
`;
const Login = () => {
  const [state, setState] = useState({
    login: '',
    password: ''
  });
  const [isCheck, setCheck] = useState(false);
  const history = useHistory();
  const handleChangeInput = name => e => {
    setState({
      ...state,
      [name]: e.target.value
    });
  };
  const handleSubmit = e => {
    setCheck(true);
    e.preventDefault();
    const { login, password } = state;
    // const matches = password.match(/\d+/g);
    // if (matches != null &&
    if (login.length > 5 && password.length > 4) {
      const send = {
        email: login,
        password
      };
      axiosInstance
        .post('/user/login', send)
        .then(res => {
          localStorage.setItem('auth', res.data.token);
          localStorage.setItem('userId', res.data.user._id);
          history.push('/user');
        })
        .catch(err => {
          setCheck(false);
        });
    } else {
      setCheck(false);
    }
  };
  const { login, password } = state;
  const theme = {
    main: 'mediumseagreen'
  };
  return (
    <>
      <ThemeProvider theme={theme}>
        <ContainerLogin autoComplete="on" onSubmit={handleSubmit}>
          <HeaderNav>Login to your Account</HeaderNav>
          <Input
            required
            type="email"
            theme={login.length > 5 ? { main: '#7faaf0' } : { main: '#f27a72' }}
            value={login}
            autocomplete="off"
            onChange={handleChangeInput('login')}
          />
          <Input
            required
            theme={password.length > 4 ? { main: '#7faaf0' } : { main: '#f27a72' }}
            type="password"
            autocomplete="new-password"
            value={password}
            onChange={handleChangeInput('password')}
          />
          <WrapperInfo>
            <Input type="checkbox" /> Keep Me logged for 30 day
          </WrapperInfo>
          {!isCheck ? (
            <LoginButton disabled={login.length < 5} onSubmit={handleSubmit}>
              Login
            </LoginButton>
          ) : (
            <Loading />
          )}
        </ContainerLogin>
      </ThemeProvider>
    </>
  );
};
export default Login;
