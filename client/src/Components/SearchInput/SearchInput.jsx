import React, { useState } from 'react';
import './SearchInput.css';
import { axiosInstance } from '../../axios';
import { useHistory } from 'react-router-dom';

const SearchInput = () => {
  const [open, setOpen] = useState(false);
  const [searchButton, setSearchButton] = useState('');
  const history = useHistory();

    const handleSubmit = e => {
    e.preventDefault();
    const send = {
        name : searchButton
    }
    axiosInstance
      .post('/api/query', send)
      .then(res => {
        console.log(res);
        if(!res.data.error)
        history.push({
            pathname : '/search',
            state : res.data
        })
          else {
              alert('Not found')
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
  return (
    <div className={open ? 'open search' : 'search'}>
      <form onSubmit={handleSubmit}>
        <input
          type="search"
          onChange={event => setSearchButton(event.target.value)}
          className={'search-box'}
        />
      </form>
      <span className="search-button" onClick={() => setOpen(!open)}>
        <span className="search-icon"></span>
      </span>
    </div>
  );
};
export default SearchInput;
