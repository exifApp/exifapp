import React from 'react';
import { useHistory } from 'react-router-dom';
import OtherImages from "../User/Dashboard/OtherImages/OtherImages";

const FindElment = () => {
  const history = useHistory();
  const { state } = history.location;
  return state ? (
    <>
      <OtherImages state={state}/>
    </>
  ) : (
    <h1>Brak danych</h1>
  );
};
export default FindElment;
