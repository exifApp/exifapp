import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Person from '@material-ui/icons/Person';
import { useHistory } from 'react-router-dom';
import Loading from '../../UI/Loading/Loading';
import { getToken } from '../../helper/helper';
import AddPhoto from './Dashboard/AddPhoto/AddPhoto';

const drawerWidth = 240;

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex'
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      color: 'white'
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginRight: 36
    },
    hide: {
      display: 'none'
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap'
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1
      }
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar
    },
    content: {
      flexGrow: 1,
      width: '100%',
      padding: theme.spacing(3)
    },
    userNav: {},
    userName: {
      textAlign: 'center'
    },
    border: {
      width: '90%',
      height: 2,
      background: '#777',
      margin: '0 auto',
      boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)'
    },
    imgUser: {
      borderRadius: '50%',
      display: 'flex',
      margin: 'auto'
    },
    logout: {}
  })
);

const User = () => {
  const classes = useStyles({});
  const history = useHistory();
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    if (getToken()) {
      setIsLoading(false);
    } else {
      history.push('/');
    }
  }, [history]);

  const handleDrawerOpen = () => {
    setIsOpen(true);
  };
  const handleDrawerClose = () => {
    setIsOpen(false);
  };
  const switchImages = index => {
    const convertIndex = index.toString();
    switch (convertIndex) {
      case '0':
        return <Person />;
      default:
        console.log('Sorry, we are out of');
    }
  };
  const handleLogout = () => {
    localStorage.removeItem('auth');
    history.push('/');
  };

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: isOpen
            })}
          >
            <Toolbar className={classes.toolbar}>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, {
                  [classes.hide]: isOpen
                })}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap>
                ExIfApp
              </Typography>
              <div className={classes.logout} onClick={handleLogout}>
                Logout
              </div>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
              [classes.drawerOpen]: isOpen,
              [classes.drawerClose]: !isOpen
            })}
            classes={{
              paper: clsx({
                [classes.drawerOpen]: isOpen,
                [classes.drawerClose]: !isOpen
              })
            }}
            open={isOpen}
          >
            <div className={classes.toolbar}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
              </IconButton>
            </div>
            <Divider />
            <List>
              <>
                {isOpen && (
                  <>
                    <div className={classes.userName}>{/* <h3>{`${name/}`}</h3> */}</div>
                    <div className={classes.border} />
                  </>
                )}
                {['Dodaj zdjecie'].map((text, index) => (
                  <ListItem button  key={text}>
                    <ListItemIcon>
                      <>{switchImages(index)}</>
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                ))}
              </>
            </List>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <AddPhoto />
          </main>
        </div>
      )}
    </>
  );
};
export default User;
