import React, { useEffect, useState } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import ModalUI from '../../../../UI/Modal/Modal';
import Maps from '../Maps/Maps';
import { axiosInstance } from '../../../../axios';

const useStyles = makeStyles(theme =>
  createStyles({
    imgContainer: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    img: {
      width: 200,
      height: 200,
      margin: 10,
      border: '1px solid #fefefe',
      borderRadius: '20px'
    },
    content: {
      flexDirection: 'column',
      flexWrap: 'wrap',
      padding: 10
    },
    modalContainer: {
      maxHeight: 300,
      minHeight: 500,
      overflowY: 'scroll'
    }
  })
);
const OtherImages = ({ type, added, state }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [isDisplayModal, setDisplayModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [cordinate, setCordinate] = useState(null);
  const classes = useStyles();

  useEffect(() => {
    if (type === 'user' || type === 'all') {
      axiosInstance.get('/images').then(res => {
        let userContent;
        if (type === 'user') {
          const filterUsers = res.data.users.filter(item => {
            if (localStorage.getItem('userId') === item.userId) {
              return item;
            }
          });
          userContent = filterUsers.map(item => {
            const toArray = {
              profileImg: item.profileImg,
              information: JSON.parse(item.data[0])
            };
            return toArray;
          });
        } else if (type === 'all') {
          userContent = res.data.users.map(item => {
            const toArray = {
              profileImg: item.profileImg,
              information: JSON.parse(item.data[0])
            };
            return toArray;
          });
        }
        setData(userContent);
        setLoading(false);
      });
    } else {
      setData(state);
      setLoading(false);
    }
  }, [added, type]);

  const handleDisplayModal = item => {
    let lng, lat;
    item.information.forEach(cor => {
      if (cor.atr === 'GPSLongitude') {
        lng = cor.desc;
      }
      if (cor.atr === 'GPSLatitude') {
        lat = cor.desc;
      }
    });
    if ((typeof lat !== 'undefined') | (typeof lng !== 'undefined')) {
      setCordinate({ lat, lng });
    } else {
      setCordinate({
        lat: 0,
        lng: 0
      });
    }
    setModalContent(item.information);
    setDisplayModal(true);
  };
  const checkExistImages = () => {
    return typeof data[0] !== 'undefined';
  };
  return (
    <>
      {!isLoading ? (
        <>
          {type === 'user' && checkExistImages() ? (
            <h1>Twoje zdjęcia</h1>
          ) : (
            <h1>Ostatnio dodane zdjęcia</h1>
          )}
          <div className={classes.imgContainer}>
            {checkExistImages() &&
              data.map(item => {
                return (
                  <img
                    alt={'img'}
                    src={item.profileImg}
                    className={classes.img}
                    onClick={() => handleDisplayModal(item)}
                  />
                );
              })}
          </div>
          {isDisplayModal && (
            <ModalUI open={isDisplayModal} close={() => setDisplayModal(false)}>
              <>
                <div className={classes.modalContainer}>
                  {modalContent.map(item => {
                    return (
                      <div className={classes.content}>
                        <>
                          <span>Attribut : {item.atr}</span>
                          <p>Value : {item.desc}</p>
                          <p>
                            Detailed information :{' '}
                            {typeof item.value !== 'number' &&
                            item.value !== null &&
                            typeof item.value !== 'string'
                              ? item.value.map(details => {
                                  return details;
                                })
                              : item.value}
                          </p>
                        </>
                      </div>
                    );
                  })}
                  {(cordinate.lat !== 0) | (cordinate.lng !== 0) && (
                    <div style={{ height: '300px', width: '100%' }}>
                      <h3>Zdjęcie zrobiono</h3>
                      <Maps cordinate={cordinate} />
                    </div>
                  )}
                </div>
              </>
            </ModalUI>
          )}
        </>
      ) : (
        <p>No images</p>
      )}
    </>
  );
};
export default OtherImages;
