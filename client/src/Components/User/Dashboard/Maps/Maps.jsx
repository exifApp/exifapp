import React from 'react';
import Marker from './MapsMarker';
import GoogleMapReact from 'google-map-react';

const Maps = ({ cordinate }) => {
  return (
    <GoogleMapReact
      bootstrapURLKeys={{ key: 'AIzaSyAmXrvr6zuUZn6cwtstO3AHITIHM9nEd5I' }}
      defaultCenter={cordinate}
      defaultZoom={13}
      yesIWantToUseGoogleMapApiInternals
    >
      <Marker
        lat={cordinate.lat}
        lng={cordinate.lng}
        name={'Tutaj zrobiono zdjęcie'}
        color={'blue'}
      />
    </GoogleMapReact>
  );
};
export default Maps;
