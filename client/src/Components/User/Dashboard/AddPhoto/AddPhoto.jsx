import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import ModalUI from '../../../../UI/Modal/Modal';
import OtherImages from '../OtherImages/OtherImages';
import Maps from '../Maps/Maps';
import {axiosInstance} from "../../../../axios";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: 'none'
  },
  list: {
    maxHeight: 300,
    overflowY: 'scroll'
  },
  headerContent: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center'
  },
  img: {
    margin: '0 auto',
    width: 300,
    height: 300
  },
  buttonAdd: {
    display: 'flex',
    margin: '4rem auto 0rem auto ',
    padding: '1rem 3rem'
  }
}));

const AddPhoto = () => {
  const classes = useStyles();
  const [displayModal, setDisplayModal] = useState(false);
  const [isAdded, setAdded] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [file, setFile] = useState(null);
  const [img, setImg] = useState(null);
  const [cordinate, setCordinate] = useState(null);
  const handleChange = e => {
    setCordinate(null);
    setIsLoading(true);
    setImg(URL.createObjectURL(e.target.files[0]));
    setFile(e.target.files[0]);
    const data = new FormData();
    data.append('file', e.target.files[0]);
    axiosInstance
      .post('/upload', data, {})
      .then(res => {
        const dataConverting = convertData(res.data.tags);
        setData(dataConverting);
      })
      .then(() => {
        setIsLoading(false);
      });
  };
  const convertData = tags => {
    const arrWithAtr = [];
    let quantity = 0;
    let lat, lng;
    for (const value of Object.values(tags)) {
      const object = Object.keys(tags);
      const currentAtr = object[quantity];
      const ValWithAtr = {
        atr: currentAtr,
        value: value.value,
        desc: value.description
      };
      if (currentAtr === 'GPSLongitude') {
        lng = value.description;
      }
      if (currentAtr === 'GPSLatitude') {
        lat = value.description;
      }
      arrWithAtr.push(ValWithAtr);
      quantity += 1;
    }
    if ((typeof lat !== 'undefined') | (typeof lng !== 'undefined')) {
      setCordinate({ lat, lng });
    } else {
      setCordinate({
        lat: 0,
        lng: 0
      });
    }

    return arrWithAtr;
  };
  const handleAddPhoto = e => {
    e.preventDefault();
    const formData = new FormData();
    const userId = localStorage.getItem('userId');
    formData.append('profileImg', file);
    formData.append('tags', JSON.stringify(data));
    formData.append('userId', userId);
    axiosInstance
      .post('/api/addImages', formData)
      .then(res => {
        console.log(res.data.message);
        if (res.data.message === 'true') {
          setAdded(true);
          setDisplayModal(false);
          setImg(null);
          setData([]);
          setAdded(false);
          setFile(null);
        }
      })
      .catch(err => console.log('error ', err));
  };
  const clearState = () => {
    setCordinate(null);
    setAdded(true);
    setDisplayModal(false);
    setImg(null);
    setData([]);
    setAdded(false);
    setFile(null);
  };

  const checkMetaData = () => {
    return data.length > 0 && isLoading === false;
  };
  return (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          if (displayModal === false) {
            clearState();
          }
          setDisplayModal(!displayModal);
        }}
      >
        Dodaj zdjecie
      </Button>
      {displayModal && (
        <ModalUI open={displayModal} close={() => setDisplayModal(false)}>
          <>
            <div className={classes.headerContent}>
              <input
                accept="image/jpg,image/jpeg"
                className={classes.input}
                id="contained-button-file"
                multiple
                onChange={e => handleChange(e)}
                type="file"
              />
              <label htmlFor="contained-button-file">
                <Button variant="contained" component="span" className={classes.button}>
                  Upload
                </Button>
              </label>
              {file !== null && <img src={img} alt="img" className={classes.img} />}
            </div>
            {checkMetaData() && (
              <>
                <div className={classes.list}>
                  {data.map(item => {
                    return (
                      <>
                        <h4>{item.atr}</h4>
                        <h5>{item.desc}</h5>
                        <p>
                          Detailed information :{' '}
                          {typeof item.value !== 'number' &&
                          item.value !== null &&
                          typeof item.value !== 'string'
                            ? item.value.map(details => {
                                return details;
                              })
                            : item.value}
                        </p>
                      </>
                    );
                  })}

                  {(cordinate.lat !== 0) | (cordinate.lng !== 0) && (
                    <div style={{ height: '300px', width: '100%' }}>
                      <h3>Zdjęcie zrobiono</h3>
                      <Maps cordinate={cordinate} />
                    </div>
                  )}
                  <Button
                    onClick={handleAddPhoto}
                    variant="contained"
                    className={classes.buttonAdd}
                    color="primary"
                  >
                    Add Photo
                  </Button>
                </div>
              </>
            )}
          </>
        </ModalUI>
      )}
      <OtherImages type={'user'} added={isAdded} />
    </>
  );
};
export default AddPhoto;
