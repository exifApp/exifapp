import React from 'react';
import styled from 'styled-components';
import SearchInput from '../SearchInput/SearchInput';
import Images from '../../assets/bestImg.jpg';

const ContainerImg = styled.img.attrs({
  src: Images
})`
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  z-index: 1;
`;
const HeaderStyle = styled.h1`
  text-align: center;
  z-index: 2;
  position: relative;
  font-family: 'Lato';
`;
const Layout = () => {
  return (
    <>
      <HeaderStyle>Wszukaj zdjęcia</HeaderStyle>
      <ContainerImg />
      <SearchInput />
    </>
  );
};
export default Layout;
