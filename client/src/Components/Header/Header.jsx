import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { getToken } from '../../helper/helper';

const Nav = styled.nav`
  display: flex;
  justify-content: flex-end;
  z-index: 2;
  position: relative;
  background-color: rgba(0, 0, 0, 0.5);
`;

const HeaderNav = styled.div`
  color: #fff;
  padding: 1em;
  margin-top: 1em;
  border-radius: 3px;
  margin: 1em;
  cursor: pointer;
  font-size: 1.1em;
  font-weight: 600;
  letter-spacing: 1.5px;
  text-decoration: none;
`;

const Header = () => {
  const [isLogged, setLogged] = useState(false);
  useEffect(() => {
    if (getToken()) {
      setLogged(true);
    } else {
      setLogged(false);
    }
  }, []);
  return (
    <Nav>
      <Link to="/">
        <HeaderNav> Home </HeaderNav>
      </Link>
      <HeaderNav> O nas </HeaderNav>
      {isLogged ? (
        <Link to="/user">
          <HeaderNav> Mój profil </HeaderNav>
        </Link>
      ) : (
        <Link to="/Login">
          <HeaderNav> Login </HeaderNav>
        </Link>
      )}
      <Link to="/Pircing">
        <HeaderNav> Pircing </HeaderNav>
      </Link>
      {!isLogged && (
        <Link to="/Register">
          <HeaderNav> Register </HeaderNav>
        </Link>
      )}
    </Nav>
  );
};
export default Header;
