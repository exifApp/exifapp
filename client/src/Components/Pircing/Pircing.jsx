import React from 'react';
import styled from 'styled-components'
import Card from '../../Containers/Card/Card'

const ContainerCard = styled.div`
  width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  background-image: linear-gradient(-20deg, #ff2846 0%, #6944ff 100%);
  padding-top: 100px;
  position: absolute;
  top: 0;
  margin: 0 auto;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Pircing = () => {
  const CardItems = [
    {
      plan: 'Standard',
      price: '$ 5.99',
      description: 'per month, billed annually or $3.99 billed monthly',
      aboutPlan: ['3 Images', '24/7 support', 'Android App', '1GB Storage']
    },
    {
      plan: 'Medium',
      price: '$ 10.99',
      description: 'per month, billed annually or $7.99 billed monthly',
      aboutPlan: ['10 Images', '24/7 support', 'Android App', '3GB Storage']
    },
    {
      plan: 'Pro',
      price: '$ 15.99',
      description: 'per month, billed annuallys or $15.99 billed monthly',
      aboutPlan: ['20 Images', '24/7 support', 'Android App', '5GB Storage']
    }
  ];
  return (
    <>
      <ContainerCard>
        {CardItems.map(createCard => {
          return <Card key={createCard.plan} item={createCard} />;
        })}
      </ContainerCard>
    </>
  );
};
export default Pircing;
