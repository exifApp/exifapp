import React, { useState } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { ContainerLogin, LoginButton } from '../Login/Login';
import {axiosInstance} from "../../axios";

const Input = styled.input`
  margin: 0.5em;
  padding: 1em;

  background-color: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
  font-size: 18px;
`;
const Header = styled.h1`
  text-align: center;
`;

const Register = () => {
  const history = useHistory();
  const [state, setState] = useState({
    email: '',
    password: '',
    typeSubscription: '',
    name: '',
    surname: '',
    numberPhone: 0
  });
  const { email, password, name, surname, numberPhone } = state;
  const handleChangeInput = name => e => {
    setState({
      ...state,
      [name]: e.target.value
    });
  };
  const checkFilledField = () => {
    return (
      numberPhone.length > 9 &&
      surname.length > 3 &&
      name.length > 3 &&
      password.length > 4 &&
      email.length > 5
    );
  };
  const handleSubmit = e => {
    e.preventDefault();
    const { email, password, name } = state;
    const send = {
      email,
      password,
      name
    };
    axiosInstance
      .post('/register', send)
      .then(res => {
        localStorage.setItem('auth', res.data.token);
        history.push('/user');
      })
      .catch(err => {
        console.log(err);
      });
  };
  return (
    <>
      <Header>Zarejstruj się</Header>
      <ContainerLogin onSubmit={e => handleSubmit(e)}>
        <Input
          required
          type="email"
          placeholder="Email"
          theme={email.length > 5 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          value={email}
          autocomplete="off"
          onChange={handleChangeInput('email')}
        />
        <Input
          required
          theme={password.length > 4 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="password"
          placeholder="Password"
          autocomplete="new-password"
          value={password}
          onChange={handleChangeInput('password')}
        />
        <Input
          required
          theme={name.length > 3 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="text"
          placeholder="Nick"
          autocomplete="new-password"
          value={name}
          onChange={handleChangeInput('name')}
        />
        <Input
          required
          theme={surname.length > 3 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="text"
          placeholder="Surname"
          autocomplete="new-password"
          value={surname}
          onChange={handleChangeInput('surname')}
        />
        <Input
          required
          theme={numberPhone.length > 9 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="number"
          placeholder="Phone number"
          autocomplete="new-password"
          value={numberPhone}
          onChange={handleChangeInput('numberPhone')}
        />
        <LoginButton
          disabled={!checkFilledField()}
          onSubmit={e => handleSubmit(e)}
          theme={
            !checkFilledField()
              ? { main: 'linear-gradient(to right, #757f9a, #d7dde8)' }
              : { main: 'linear-gradient(to right, #11998e, #38ef7d)' }
          }
        >
          Rejstracja
        </LoginButton>
      </ContainerLogin>
    </>
  );
};
export default Register;
