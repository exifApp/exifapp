import React from 'react';
import styled from 'styled-components';
import Image1 from '../../assets/jakob.jpg';
import Image2 from '../../assets/radek.jpg';
import Image3 from '../../assets/am.jpg';

const ContainerImg3 = styled.img.attrs({
  src: Image3
})`
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  z-index: 1;
  opacity: 0.8;
`;
const ContainerImg1 = styled.img.attrs({
  src: Image1
})`
  height: 50%;
  width: 50%;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  display: block;
  border: 1px solid black;
  z-index: 1;
`;
const ContainerImg2 = styled.img.attrs({
  src: Image2
})`
  height: 50%;
  width: 50%;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  display: block;
  z-index: 1;
`;
const HeaderStyle = styled.h1`
  text-align: center;
  z-index: 2;
  position: relative;
  font-family: 'Lato';
`;
const ContainerJakub = styled.h2`
  text-align: left;

  width: 50%;
  display: inline-block;
  box-sizing: border-box;
`;
const ContainerRadek = styled.h2`
  text-align: left;
  width: 50%;

  display: inline-block;
  box-sizing: border-box;
`;
const ContainerText = styled.p`
  z-index: 11;
  position: relative;
`;
const AboutUs = () => {
  return (
    <>
      <HeaderStyle>O Nas!</HeaderStyle>
      <ContainerImg3 />

      <ContainerJakub>
        <ContainerImg1 />
        <HeaderStyle>Jakub Kowalski</HeaderStyle>
        <ContainerText>
          Student IV roku Informatyki na Akademii Morskiej. Programowanie aplikacji www jest jego
          pasją, ale również pracą. Wolne chwile wykorzystuje w aktywny sposób. Uczęszcza na
          siłownie, podróżuje oraz uwielbia spędzać czas ze znajomymi.
        </ContainerText>
      </ContainerJakub>
      <ContainerRadek>
        <ContainerImg2 />
        <HeaderStyle>Radosław Russ</HeaderStyle>
        <ContainerText>
          Student IV roku Informatyki na Akademii Morskiej. Uwielbia sport ,zwłaszcza piłkę nożną
          oraz siatkówkę. Jego prawdziwą pasją jednak jest spanie.W wolnych chwilach pochłania go
          słuchanie muzyki,gotowanie.
        </ContainerText>
      </ContainerRadek>
    </>
  );
};
export default AboutUs;
