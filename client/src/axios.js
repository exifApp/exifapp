import axios from 'axios';

const currentURL = 'http://localhost:3001';
export const axiosInstance = axios.create({
  baseURL: currentURL,
  proxy: false
});
