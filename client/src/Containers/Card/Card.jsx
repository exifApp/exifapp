import React from 'react';
import styled from 'styled-components'

const Container = styled.div`
  height : 450px;
  width: 25%;
  text-align : center;
  box-shadow: 0px 8px 60px -10px rgba(13,28,39,0.6);
  background: #fff;
  margin: 2em;
  font-weight: 500;
  color: #324e63;
  &.active {
      filter: blur(6px);
    };
  &:nth-child(2) {
    transform : scale(1.1)    
  }
  @media (max-width: 768px) {
    width : 80%;
  }
`
const DivTop = styled.div`
  height: 10px;
  background: #667db6;
  background: -webkit-linear-gradient(to right, #667db6, #0082c8, #0082c8, #667db6);  /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #667db6, #0082c8, #0082c8, #667db6); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
`
const UserPlan = styled.div`
  margin : 10px auto;
  background: #24C6DC;  /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #514A9D, #24C6DC);  /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #514A9D, #24C6DC); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  border-radius: 10px
  width: fit-content;
  padding: 0.2em 1em;
  height: auto;
  color: #fefefe;
  font-weight: 700;
`
const Price = styled.h1`
  font-weight : 800;
`
const PriceDescription = styled.p`
  font-weight : 300;
  font-size: 12px;
  padding : 0px 5em;
`
const Border = styled.p`
  width: 90%;
  margin: 0 auto;
  height: 3px;
  background: azure;
`
const AboutPackage = styled.div`
padding : 10px
`
const Button = styled.button`
background: linear-gradient(45deg, #1da1f2, #0e71c8);
box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.4);
width: 50%;
height: 10%;
font-size: 18px;
border-radius: 10px;
text-decoration: none;
cursor: pointer;
backface-visibility: hidden;
transition: all .3s;
margin: 2em 0em;
color : white;
font-weight : 500;
padding : 0.3em;
 &:hover {
  box-shadow: 0px 7px 30px rgba(19, 127, 212, 0.75);
  transform: translateY(-5px);
}

`

const Card = ({ item }) => {
  const { plan, price, aboutPlan, description } = item
  return (
    <Container>
      <DivTop />
      <UserPlan> {plan} </UserPlan>
      <Price> {price} </Price>
      <PriceDescription>
        {description}
      </PriceDescription>
      <Border />
      {aboutPlan.map((aboutItem, key) => {
        return <AboutPackage key={key}>
          {aboutItem}
        </AboutPackage>
      })}
      <Button> Select </Button>
    </Container>
  )
}
export default Card;