import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Layout from './Components/Layout/Layout';
import Login from './Components/Login/Login';
import Pircing from './Components/Pircing/Pircing';
import Register from './Components/Register/Register';
import Header from './Components/Header/Header';
import User from './Components/User/User';
import styled from 'styled-components';
import OtherImages from './Components/User/Dashboard/OtherImages/OtherImages';
import Footer from './Components/Footer/Footer';
import FindElment from './Components/FindElement/FindElement';
import AboutUs from './Components/AboutUs/AboutUs';
const ContentLayout = styled.div`
  height: 100vh;
`;
const UsersImages = styled.div`
  width: 80%;
  margin: 0 auto;
`;

export default function Router() {
  return (
    <BrowserRouter>
      <Route exact path="/">
        <>
          <ContentLayout>
            <Header />
            <Layout />
          </ContentLayout>
          <UsersImages>
            <OtherImages type={'all'} />
          </UsersImages>
          <Footer />
        </>
      </Route>
      <Route path="/search">
        <ContentLayout>
          <Header />
          <FindElment />
        </ContentLayout>
      </Route>

      <Route path="/Login">
        <Header />
        <Login />
      </Route>
      <Route exact path="/Pircing">
        <Header />
        <Pircing />
      </Route>
      <Route path="/AboutUs">
        <Header />
        <AboutUs />
      </Route>
      <Route exact path="/Register">
        <Header />
        <Register />
      </Route>
      <Route exact path="/User">
        <User />
      </Route>
    </BrowserRouter>
  );
}
