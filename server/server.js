const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");

const db = require("./src/database/index");
const userRoute = require("./src/router/exif-router");

const app = express();
const apiPort = process.env.PORT || 3001 ;
app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(cors());
app.use(bodyParser.json());
app.use("/public", express.static("public"));

app.use(express.static(path.join(__dirname, "../client/build")));
if (process.env.NODE_ENV === "production") {
	app.use(express.static("../client/build"));

	app.get("*", (request, response) => {
		response.sendFile(
			path.resolve(__dirname, "..", "client", "build", "index.html")
		);
	});
}
// res.sendFile(path.join(__dirname + "../client/build/index.html"));

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(userRoute);

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));
