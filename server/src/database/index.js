const mongoose = require("mongoose");
require("dotenv").config();

const uri =
	"mongodb+srv://" +
	`${process.env.DB_USER}` +
	":" +
	`${process.env.DB_PASS}` +
	"@" +
	`${process.env.DB_NAME}` +
	"-myswk.mongodb.net/test?retryWrites=true&w=majority";
mongoose
	.connect(uri, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useUnifiedTopology: true
	})
	.then(() => {
		console.log("Connect dev ok");
	})
	.catch((e) => {
		console.error("Connection dev error", e.message);
	});

const db = mongoose.connection;

module.exports = db;
