const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const userSchema = mongoose.Schema({
	name: {
		type: String,
		trim: true
	},
	email: {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		validate: (value) => {
			if (!validator.isEmail(value)) {
				throw new Error({ error: "Invalid Email address" });
			}
		}
	},
	password: {
		type: String,
		required: true,
		minLength: 7
	},
	tokens: [
		{
			token: {
				type: String,
				required: true
			}
		}
	]
});

userSchema.pre("save", async function(next) {
	// Hash the password before saving the user model
	const user = this;
	if (user.isModified("password")) {
		user.password = await bcrypt.hash(user.password, 8);
	}
	next();
});

userSchema.methods.generateAuthToken = async function() {
	// Generate an auth token for the user
	const user = this;
	const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY);
	user.tokens = user.tokens.concat({ token });
	await user.save();
	return token;
};
userSchema.statics.generateToken = async function() {
	const user = this;
	const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY);
	return token;
};
userSchema.statics.findUser = async (email, password) => {
	const user = await User.findOne({
		email
	});
	const isPasswordMatch = await bcrypt.compare(password, user.password);
	console.log(isPasswordMatch);
	if (!isPasswordMatch) {
		return false;
	} else {
		return user;
	}
};
userSchema.statics.findByCredentials = async (email, password) => {
	const users = await User.findOne({ email });
	if (users) {
		return users;
	}
	if (users === null) {
		console.log(users, "null");
		return false;
	}
	if (!users) {
		console.log("!users");
		throw new Error({ error: "Invalid login credentials" });
	}
	const isPasswordMatch = await bcrypt.compare(password, user.password);
	if (!isPasswordMatch) {
		throw new Error({ error: "Invalid login credentials" });
	}
	return users;
};
const User = mongoose.model("User", userSchema);
module.exports = User;
