const express = require("express");
const multer = require("multer");
const ExifReader = require("exifreader");
const mongoose = require("mongoose");

const UserCtrl = require("../user/userModel");

const router = express.Router();

// router.post("/user", UserCtrl.createUser);
// router.get("/getAllUsers", UserCtrl.getUser);
router.post("/register", async (req, res) => {
  try {
    const { email, password } = req.body;
    const userek = await UserCtrl.findByCredentials(email, password);
    if (!userek) {
      const user = new UserCtrl(req.body);
      await user.save();
      const token = await user.generateAuthToken();
      res.status(201).send({ user, token });
    } else {
      res.status(401).send({ error: "User exists" });
    }
  } catch (err) {
    res.status(400).send(err);
  }
});

router.post("/user/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await UserCtrl.findUser(email, password);
    if (!user) {
      return res.status(401).send({ error: "Login failed !" });
    }
    const token = await UserCtrl.generateToken();
    res.status(201).send({ user, token });
  } catch (error) {
    res.status(400).send(error);
  }
});

var uploadTags = multer({ storage: storageImg }).single("file");

var storageImg = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public");
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

router.post("/upload", async (req, res) => {
  uploadTags(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err);
    } else if (err) {
      return res.status(500).json(err);
    }
    const tags = ExifReader.load(req.file.buffer);
    return res.status(201).send({ tags });
  });
});

const DIR = "./public/";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname
      .toLowerCase()
      .split(" ")
      .join("-");
    cb(null, fileName);
  }
});

var upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
    }
  }
});
let UserAddedImages = require("../images/imagesModel");
router.post("/api/addImages", upload.single("profileImg"), (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const imageAddCollect = new UserAddedImages({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    profileImg: url + "/public/" + req.file.filename,
    data: req.body.tags,
    userId: req.body.userId
  });
  imageAddCollect
    .save()
    .then(result => {
      res.status(201).json({
        message: "true",
        userCreated: {
          _id: result._id,
          profileImg: result.profileImg,
          data: result.data,
          userId: result.userId
        }
      });
    })
    .catch(err => {
      console.log(err),
        res.status(500).json({
          error: err
        });
    });
});
const imgCollect = require("../images/imagesModel");
const NodeGeocoder = require("node-geocoder");

router.post("/api/query", async (req, res) => {
  try {
    const name = req.body.name.toLowerCase();
    const userek = await imgCollect.find({});
    const parse = await userek.map(item => {
      const toArray = {
        profileImg: item.profileImg,
        information: JSON.parse(item.data[0])
      };
      return toArray;
    });
    let arr = [];
    for (let index in parse) {
      const finalFindIndex = parse[index].information.findIndex(
        indexId => indexId.atr === "Make" && indexId.desc.toLowerCase() === name
      );
      if (finalFindIndex !== -1) {
        arr.push(parse[index]);
      }
    }
    if (arr.length === 0) {
      const options = {
        provider: "google",
        httpAdapter: "https",
        apiKey: "AIzaSyAmXrvr6zuUZn6cwtstO3AHITIHM9nEd5I",
        formatter: null
      };
      const geocoder = NodeGeocoder(options);
      geocoder.geocode(name, function(err, resp) {
        const minlat = resp[0].latitude - 0.3;
        const maxlat = resp[0].latitude + 0.3;
        const minLng = resp[0].longitude - 0.3;
        const maxLng = resp[0].longitude + 0.3;

        const findElement = [];
        for (let index in parse) {
          let latOk = false;
          let lngOk = false;
          const x = parse[index].information.forEach(indexId => {
            const parseValue = parseFloat(indexId.desc);
            if (
              indexId.atr === "GPSLatitude" &&
              Math.floor(minlat * 100) < Math.floor(parseValue * 100) &&
              Math.floor(parseValue * 100) < Math.floor(maxlat * 100)
            ) {
              latOk = true;
            } else if (
              indexId.atr === "GPSLongitude" &&
              Math.floor(minLng * 100) < Math.floor(parseValue * 100) &&
              Math.floor(parseValue * 100) < Math.floor(maxLng * 100)
            ) {
              lngOk = true;
            }
            return true;
          });
          if (latOk === true && lngOk === true) {
            (latOk = false), (lngOk = false), findElement.push(parse[index]);
          }
        }
        res.status(201).send(findElement);
      });
    } else {
      res.status(201).send(arr);
    }
  } catch (err) {
    res.status(400).send(err);
  }
});
router.get("/images", (req, res, next) => {
  UserAddedImages.find().then(data => {
    res.status(201).json({
      message: "ok",
      users: data
    });
  });
});
module.exports = router;
