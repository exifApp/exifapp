const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const imageSchema = new Schema({
	_id: mongoose.Schema.Types.ObjectId,
	profileImg: {
		type: String
	},
	data: {
		type : Array
	},
	userId : {
		type : String
	}
});
const Image = mongoose.model("Image", imageSchema);
module.exports = Image;
